-- GENERATED CODE
-- Node Box Editor, version 0.8.1 - Glass
-- Namespace: test
-- Platform no --

minetest.register_node("train_map:sign_dark", {
	tiles = {
		"trainMap_dark_bg.png"
	},
	paramtype = "light",
	paramtype2 = "facedir",
	drawtype = "nodebox",
	groups = {crumbly=3,},
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, 0.4375, 0.5, 0.5, 0.5}, -- NodeBox1

		},
	},
})

minetest.register_node("train_map:sign1", {
	tiles = {
		"trainMap_dark_bg.png^trainMap_1.png"
	},
	paramtype = "light",
	paramtype2 = "facedir",
	drawtype = "nodebox",
	groups = {crumbly=3,},
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, 0.4375, 0.5, 0.5, 0.5}, -- NodeBox1

		},
	},
})


minetest.register_node("train_map:sign2", {
	tiles = {
		"trainMap_dark_bg.png^trainMap_2.png"
	},
	paramtype = "light",
	paramtype2 = "facedir",
	drawtype = "nodebox",
	groups = {crumbly=3,},
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, 0.4375, 0.5, 0.5, 0.5}, -- NodeBox1

		},
	},
})

minetest.register_node("train_map:sign3", {
	tiles = {
		"trainMap_dark_bg.png^trainMap_3.png"
	},
	paramtype = "light",
	paramtype2 = "facedir",
	drawtype = "nodebox",
	groups = {crumbly=3,},
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, 0.4375, 0.5, 0.5, 0.5}, -- NodeBox1

		},
	},
})

minetest.register_node("train_map:sign4", {
	tiles = {
		"trainMap_dark_bg.png^trainMap_4.png"
	},
	paramtype = "light",
	paramtype2 = "facedir",
	drawtype = "nodebox",
	groups = {crumbly=3,},
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, 0.4375, 0.5, 0.5, 0.5}, -- NodeBox1

		},
	},
})

-- platform sign --

minetest.register_node("train_map:platform", {
	tiles = {
		"trainMap_dark_bg.png^trainMap_platform.png"
	},
	paramtype = "light",
	paramtype2 = "facedir",
	drawtype = "nodebox",
	paramtype = "light",
	groups = {crumbly=3,},
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, 0.4375, 0.5, -0.1875, 0.5}, -- NodeBox1
		},
	},
})

minetest.register_node("train_map:info", {
	tiles = {
		"trainMap_dark_bg.png^trainMap_infomation.png"
	},
	paramtype = "light",
	paramtype2 = "facedir",
	drawtype = "nodebox",
	paramtype = "light",
	groups = {crumbly=3,},
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, 0.4375, 0.5, -0.1875, 0.5}, -- NodeBox1
		},
	},
})

minetest.register_node("train_map:banner", {
	tiles = {
		"trainMap_dark_bg.png"
	},
	paramtype = "light",
	paramtype2 = "facedir",
	drawtype = "nodebox",
	paramtype = "light",
	groups = {crumbly=3,},
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, 0.4375, 0.5, -0.1875, 0.5}, -- NodeBox1
		},
	},
})

-- nomal map --


minetest.register_node("train_map:map_left", {
	tiles = {
		"trainMap_light_bg.png^trainMap_left.png"
	},
	paramtype = "light",
	paramtype2 = "facedir",
	drawtype = "nodebox",
  light_source= 3;
	groups = {crumbly=3,},
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, 0.4375, 0.5, 0.5, 0.5}, -- NodeBox1

		},
	},
})

minetest.register_node("train_map:map_right", {
	tiles = {"trainMap_light_bg.png^trainMap_right.png"
	},
	paramtype = "light",
	paramtype2 = "facedir",
	drawtype = "nodebox",
	paramtype = "light",
  light_source= 3;
	groups = {crumbly=3,},
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, 0.4375, 0.5, 0.5, 0.5}, -- NodeBox1

		},
	},
})
-- inner train map --

minetest.register_node("train_map:train_map_left", {
	tiles = {
		"default_gold_block.png",

		"default_gold_block.png",


		"default_gold_block.png",
		"default_gold_block.png",
		"default_gold_block.png",
		"default_gold_block.png^trainMap_left.png"
	},
  paramtype = "light",
	paramtype2 = "facedir",
	drawtype = "nodebox",
	paramtype = "light",
	light_source= 5;
  groups = {crumbly=3,},
	node_box = {
		type = "fixed",
		fixed = {
			{-0.4375, -0.5, 0.4375, 0.5, 0.5, 0.5}, -- NodeBox1
			{-0.5, -0.5, -0.5, -0.4375, 0.5, 0.0}, -- NodeBox2
		}
	}
})

minetest.register_node("train_map:train_map_right", {
	tiles = {
		"default_gold_block.png",
		"default_gold_block.png",
		"default_gold_block.png",
		"default_gold_block.png",
		"default_gold_block.png",
		"default_gold_block.png^trainMap_right.png"
	},
  paramtype = "light",
	paramtype2 = "facedir",
	drawtype = "nodebox",
	paramtype = "light",
	light_source= 5;
  groups = {crumbly=3,},
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, 0.4375, 0.4375, 0.5, 0.5}, -- NodeBox1
			{0.4375, -0.5, -0.5, 0.5, 0.5, 0.0}, -- NodeBox2
		}
	}
})
